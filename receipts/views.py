from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# funtion only works if user is logged in
@login_required
def show_home_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/home.html", context)


# funtion only works if user is logged in
@login_required
def create_receipt(request):
    if request.method == "POST":
        # use form to validate values and save to database
        form = ReceiptForm(request.POST)
        if form.is_valid():

            # save all information from form into variable, and not the database
            receipt = form.save(False)
            # add logged in user as the author attribute in recipe
            receipt.purchaser = request.user
            # save author with user info
            receipt.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("home")
    else:
        # create an instance of the Django model form class
        form = ReceiptForm()
        # put the form in the context
        context = {"form": form}
        return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/category.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        # use form to validate values and save to database
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            # save all information from form into variable, and not the database
            category = form.save(False)
            # add logged in user as the author attribute in recipe
            category.owner = request.user
            # save owner with user info
            category.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("category_list")
    else:
        # create an instance of the Django model form class
        form = ExpenseCategoryForm()
        # put the form in the context
        context = {
            "form": form,
        }
    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        # use form to validate values and save to database
        form = AccountForm(request.POST)
        if form.is_valid():
            # save all information from form into variable, and not the database
            account = form.save(False)
            # add logged in user as the author attribute in recipe
            account.owner = request.user
            # save owner with user info
            account.save()
            # if all is well, redirect browser to other page and leave function
            return redirect("account_list")
    else:
        # create an instance of the Django model form class
        form = AccountForm()
        # put the form in the context
        context = {
            "form": form,
        }
    return render(request, "receipts/createaccount.html", context)
