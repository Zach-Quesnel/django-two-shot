from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth.models import User


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                # log in the user
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


# Form without a model:
def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                # create new user with those values
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                # log in the user
                login(request, user)

                return redirect("home")

            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
